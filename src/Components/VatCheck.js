import React from 'react';
import DataEntry from './DataEntry';
import Request from 'react-http-request';
import './VatCheck.css';

class VatCheck extends React.Component {
  constructor(props) {
    super(props)


    this.state = {
      countryCode: 'EE',
      vatNumber: '100247019',
      name: 'Selver AS',
      address: 'Pärnu mnt 238   11624 Nõmme linnaosa Tallinn',
      valid: true,
      vatMiss: "",
      url: 'https://vat.erply.com/numbers?vatNumber='

    }
  }


  handleNumberReset = (vatMiss) => {
    console.log(vatMiss);
    var url1 = 'https://vat.erply.com/numbers?vatNumber=' + vatMiss;
    this.setState({
      url: url1
      });
   
  }



  render() {

    const isValid = this.state.valid;
    return (<section className="hero is-primary is-bold is-fullheight has-text-centered">
              <div className="hero-body">
                <div className="container">
                  <h1 className="title is-size-1">
                    VAT number check
                  </h1>
                  <DataEntry onNumberReset={this.handleNumberReset}/>
                  {isValid ? (
                    <Request
                      url={this.state.url}
                      method='get'
                      accept='application/json'
                      verbose={true}
                    >
                      {
                        ({error, result, loading}) => {
                          if (loading) {
                            return <div className="lds-dual-ring"></div>;
                          } else {
                            if (result.body.Valid) {
                            return <div className="section">
                              <nav className="level">
                                <div className="level-item has-text-centered">
                                  <div>
                                    <p className="heading is-size-6">Country code</p>
                                    <p className="is-size-4">{result.body.CountryCode}</p>
                                  </div>
                                </div>
                                <div className="level-item has-text-centered">
                                  <div>
                                    <p className="heading is-size-6">VAT number</p>
                                    <p className="is-size-4">{result.body.VATNumber}</p>
                                  </div>
                                </div>
                                <div className="level-item has-text-centered">
                                  <div>
                                    <p className="heading is-size-6">Name</p>
                                    <p className="is-size-4">{result.body.Name}</p>
                                  </div>
                                </div>
                                <div className="level-item has-text-centered">
                                  <div>
                                    <p className="heading is-size-6">Address</p>
                                    <p className="is-size-4">{result.body.Address}</p>
                                  </div>
                                </div>
                              </nav>
                            </div>;
                          } else {
                            return <div className="section">
                             
                                <div className="level-item has-text-centered">
                                  
                                    <p className="is-size-4">Enter valid VAT number</p>
                                  
                                </div>
                            </div>;

                          }
                        }
                      }
                    }
                  </Request>
                  
                    ) : (
                    <h2 className="">
                    Enter valid VAT number
                  </h2>
                  )}

                </div>
              </div>
            </section>

            )
          }
  }

  export default VatCheck